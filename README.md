# Signed by User

This provides simple additional tokens for [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) inserting signature data and other contact information for the currently logged in user.


## Installation
* Install [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) extension if not yet done
* Clone this repo into the subfolder *tokens* in your Custom PHP directory (or save them manually in this directory) – e.g. *uploads/civicrm/custom_php/tokens*
* Activate added tokens in *Administer | Communications | Endabled Tokens*

## Usage
Use <code>{signedbyuser.signature_text}</code> and <code>{signedbyuser.signature_html}</code> to print a contact's primary signature data in either plain text or HTML. 

Likewise you can use <code>{signedbyuser.name}</code> (display name), <code>{signedbyuser.function}</code> (function), <code>{signedbyuser.email}</code> (primary email) or <code>{signedbyuser.phone}</code> (primary phone) to create signatures, letterheads etc. that are dependent on the current user.

## License
The extension is licensed under [AGPL-3.0](LICENSE.txt).
