<?php
/**
 * Sammlung von Token zum Einfügen von Signaturdaten 
 * der aktuell eingeloggten Nutzer·in
 * 
 * Collection of tokens for inserting signature data 
 * for the currently logged in user
 * 
 * Copyright (C) 2022  civiservice.de GmbH (info@civiservice.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Declare tokens
function signedbyuser_civitoken_declare($token){
  return array(
    $token. '.name' => 'Name Unterzeichnende',
    $token. '.function' => 'Funktion Unterzeichnende',
    $token. '.email' => 'primäre E-Mail Unterzeichnende',
    $token. '.phone' => 'primäre Tel. Unterzeichnende',
    $token. '.signature_html' => 'E-Mail-Signatur (html) Unterzeichnende',
    $token. '.signature_text' => 'E-Mail-Signatur (Text) Unterzeichnende',
  );
}

function signedbyuser_civitoken_get($cid, &$value){

    // Get contact_id of logged-in user 
    $currentuser = CRM_Core_Session::singleton()->getLoggedInContactID();

    // Get name, email, phone and job_title of logged-in user via API
    $result = civicrm_api3('Contact', 'get', array(
      'sequential' => 1,
      'return' => "display_name,job_title,email,phone",
      'id' => $currentuser,
    ));

    if(isset($result['values'][0]['display_name'])) {
      $display_name = $result['values'][0]['display_name'];
    } else $display_name = "";

    if(isset($result['values'][0]['job_title'])) {
      $job_title = $result['values'][0]['job_title'];
    } else $job_title = "";

    if(isset($result['values'][0]['email'])) {
      $email = $result['values'][0]['email'];
    } else $email = "";

    if(isset($result['values'][0]['phone'])) {
      $phone = $result['values'][0]['phone'];
    } else $phone = "";
		
    // Get email signature (html) from primary email address of the logged-in contact
    $result = civicrm_api3('Email', 'get', [
      'sequential' => 1,
      'return' => ["signature_html", "signature_text", "is_primary"],
      'contact_id.id' => $currentuser,
      'is_primary' => 1,
    ]);

    if(isset($result['values'][0]['signature_html'])) {
      $signature_html = $result['values'][0]['signature_html'];
    } else $signature_html = "";
    
    if(isset($result['values'][0]['signature_text'])) {
      $signature_text = $result['values'][0]['signature_text'];
    } else $signature_text = "";

    // Fill in values to tokens
    $value['signedbyuser.name'] = $display_name;
    $value['signedbyuser.email'] = $email;
    $value['signedbyuser.phone'] = $phone;
    $value['signedbyuser.function'] = $job_title;
    $value['signedbyuser.signature_html'] = $signature_html;
    $value['signedbyuser.signature_text'] = $signature_text;
}
